### Abschlussprojekt pfuschDB
(in Arbeit, momentane Stand im dev branch)

von Danijel Simic, Ideal Tertini und Fanol Ismajili

## Projektdefinition
Die Plattform pfuschdb soll es Menschen aller Altersgruppen ermöglichen, ihre Kenntnisse
anzubieten und ihre Fähigkeiten zu vermarkten, während Benutzer qualifizierte Fachkräfte
finden und bewerten können. Sie dient als Brücke zwischen talentierten Individuen, die auf
der Suche nach einer Plattform sind, um ihre Dienstleistungen anzubieten, und Benutzern, die
spezifische Bedürfnisse oder Projekte haben und dafür qualifizierte Fachkräfte suchen. Die
Benutzerfreundlichkeit steht im Vordergrund, sodass die Plattform so gestaltet ist, dass sie
intuitiv zu navigieren ist, wodurch Benutzer schnell und unkompliziert die passenden
Fachkräfte finden können. Ein integriertes Bewertungssystem ermöglicht es den Benutzern,
Feedback zu hinterlassen, was wiederum zur Qualitätssicherung und Transparenz beiträgt.
Diesfördert ein vertrauensvolles Umfeld, in dem Fachwissen und Kompetenz im Mittelpunkt
stehen.