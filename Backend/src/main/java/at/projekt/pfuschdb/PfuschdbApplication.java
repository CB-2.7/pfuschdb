package at.projekt.pfuschdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PfuschdbApplication {

    public static void main(String[] args) {
        SpringApplication.run(PfuschdbApplication.class, args);
    }

}
